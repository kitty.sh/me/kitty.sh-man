SRC_DIR := src
PUBLISH_DIR := publish

TITLE := katherine.sh

CRI_DISTRO := $(or $(CRI_DISTRO),debian)
CRI_WORKDIR := /app
CRI_IMAGE := $(or $(CRI_IMAGE),kitty.sh-man)
CRI_TAG := $(or $(CRI_TAG),latest)
CRI_REF := $(CRI_IMAGE):$(CRI_TAG)
CRI_RUNTIME := $(notdir $(shell command -v podman || command -v docker))
CRI_FLAGS := -v "$(PWD)/publish":"$(CRI_WORKDIR)/publish"

.PHONY: all
all: $(PUBLISH_DIR)/index.html $(PUBLISH_DIR)/main.css $(PUBLISH_DIR)/katherine.8.gz

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

.PHONY: site
site: $(PUBLISH_DIR)/index.html $(PUBLISH_DIR)/main.css $( ## Create site files (index.html main.css)

.PHONY: man
man: $(PUBLISH_DIR)/katherine.8.gz ## Create katherine.8.gz

.PHONY: clean
clean: ## Clean all artifacts
	rm -rf $(PUBLISH_DIR) .dockerignore

# CONTAINER

.PHONY: %-in-container
%-in-container: container mkdir check-cri ## Run target in a container (e.g. all-in-container)
	$(CRI_RUNTIME) run --rm --user $(UID):$(GID) $(CRI_FLAGS) $(CRI_REF) make $(@:-in-container=)

.PHONY: container
container: Containerfile.$(CRI_DISTRO) .containerignore check-cri ## Build image with req. tools
	$(CRI_RUNTIME) build -f $< . -t $(CRI_REF)

.PHONY: check-cri
check-cri:
ifdef CRI_RUNTIME
	@echo "Using CRI runtime: $(CRI_RUNTIME)"
else
	@echo "Could not find podman or docker on system."
	@exit 1
endif
ifeq ($(CRI_RUNTIME), docker)
	@#printf '%s\n' Containerfile* | xargs -i sh -c 'ln -sf "$$1" "$${1/Container/Docker}"' - '{}'
	printf '%s\n' .containerignore | xargs -i sh -c 'ln -sf "$$1" "$${1/container/docker}"' - '{}'
endif

# HTML

$(PUBLISH_DIR)/index.html: $(SRC_DIR)/index.html.j2 $(PUBLISH_DIR)/index.html.fragment pip $(SRC_DIR)/templater.py
	. venv/bin/activate; python src/templater.py --title $(TITLE) --base $(word 1,$^) --content $(word 2,$^) > $@

$(PUBLISH_DIR)/index.html.fragment: $(SRC_DIR)/katherine.8 mkdir
	mandoc -Thtml -Ofragment $< > $@

.PHONY: pip
pip: venv requirements.txt
	. venv/bin/activate; pip install -r $(word 2,$^)

venv:
	python3 -m venv venv

$(PUBLISH_DIR)/main.css: $(SRC_DIR)/main.css mkdir
	cp $< $@

# ASCII

$(PUBLISH_DIR)/katherine.8.gz: $(PUBLISH_DIR)/katherine.8
	gzip --keep --force $<

$(PUBLISH_DIR)/katherine.8: $(SRC_DIR)/katherine.8 mkdir
	mandoc -Tascii $< > $@

# UTILITIES

.PHONY: mkdir
mkdir:
	mkdir -p $(PUBLISH_DIR)

# MISC

# groff: looks bad
# pandoc: looks nice, nothing is linked
# man: same as groff
# mandoc: looks nice, all titles and categories are links
# man2html: too much fluff, has index, actual links are linked
multi: mkdir ## Try out different processors
	groff -mandoc -Thtml src/katherine.8 > publish/groff.html
	pandoc --from man --to html < src/katherine.8 > publish/pandoc.html
	man --html=cat src/katherine.8 > publish/man.html
	mandoc -Thtml src/katherine.8 > publish/mandoc.html
	man2html src/katherine.8 > publish/man2html.html
