# kitty.sh-man

Repository for the landing page katherine.sh (and kitty.sh)

Built from a custom man (troff) file and mandoc (from BSD) to generate a html file

## Usage

```shell
# Make all artifacts
make

# Make all artifacts using containers (no need for dependencies)
make all-in-container

# See more options
make help
```
