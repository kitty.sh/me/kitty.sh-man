"""templater.py takes a base jinja2 template and a content file,
and prints the result to stdout.
"""
import argparse
import re
from jinja2 import Template


def templater(args):
    template = Template(args.base.read())
    html = template.render(title=args.title, content=args.content.read().rstrip())

    # Using simpler pattern, as currently all urls are encased within <i> </i>
    # pattern = r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    pattern = r'<i>(http[s]?://[A-z./-]*)</i>'
    replace = r'<i><a href="\1">\1</a></i>'

    html = re.sub(pattern, replace, html)

    # TODO: mailto link for email
    pattern = r'<i>([A-z]*@[A-z.]*)</i>'
    replace = r'<i><a href="mailto:\1">\1</a></i>'

    html = re.sub(pattern, replace, html)

    print(html)


if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description='')
    PARSER.add_argument(
        '--title',
        required=True,
        help='page title')
    PARSER.add_argument(
        '--base',
        required=True,
        type=argparse.FileType('r'),
        help='base jinja2 template')
    PARSER.add_argument(
        '--content',
        required=True,
        type=argparse.FileType('r'),
        help='content block text')
    ARGS = PARSER.parse_args()

    templater(ARGS)
